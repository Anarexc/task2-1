<?php

declare(strict_types=1);
define('ROOT_PATH', dirname(__FILE__));
//$file = fopen(ROOT_PASS . "/files/text.php", "r");

if (is_writable(ROOT_PATH . "/files/test.txt")) { //is_writable проверяет имеим ли мы доступ к файлу
    $file = fopen(ROOT_PATH . "/files/test.txt", "a+");
    if (!$file) { // проверяем можем ли мы его открыть
        die("File open error");
    }
    fwrite($file, "dasada");//записываем в файл
    fseek($file, 0);// fseek указатель в нужное место файла (в нашем случае в начало)
    while (!feof($file)) { // feof проверяет закончился файл или нет
        $line = fgets($file);// fgets выводит только одну строку файла (с помощью цыкла мы выводим весь файл)
        echo $line . "<br>";
    }
    fclose($file);// fclose закрывает файл чтобы он не висел открытым и другие смогли иметь к нему доступ
}

